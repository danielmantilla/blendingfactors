\documentclass[12pt]{article}

\usepackage[longnamesfirst]{natbib}
\usepackage{rotating,amssymb,amsmath,amsfonts,delarray}
% \usepackage[hscale=0.7,vscale=0.8]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{setspace}
\usepackage[]{graphicx}
\usepackage[]{color}
\usepackage{pgfplots}
\usepackage{hyperref}

%\setlength{\oddsidemargin}{0.1in}
%\setlength{\topmargin}{-0.5in}
%\setlength{\textwidth}{6.45in} \textheight=9.in

\usepackage{geometry}
 \geometry{
 letterpaper,
 %total={210mm,297mm},
 left=29mm,
 right=29mm,
 top=10mm,
 bottom=20mm,
 }

\newcommand{\argmax}[2]{%
\smash{\mathop{{\rm argmax}}\limits_{#1}}\, #2 }

\newcommand{\diagdots}[3][-25]{%
  \rotatebox{#1}{\makebox[0pt]{\makebox[#2]{\xleaders\hbox{$\cdot$\hskip#3}\hfill\kern0pt}}}%
}

\DeclareMathOperator{\sgn}{sign}

\DeclareMathOperator*{\Max}{Max}


\title{Blending Factors\thanks{We thank Monty Joshi for many insightful ideas and discussions.}}

\author{Andrew Ang\footnote{Ann F. Kaplan Professor of Business at Columbia University. \textbf{Email}: aa610@columbia.edu} \and Daniel Mantilla-Garcia\footnote{Head of research at Optimal Asset Management and research associate at Edhec-Risk Institute. \textbf{Email}: daniel@optimalam.com, \textbf{Address}: Optimal Asset Management Inc., 171 Main St \#298, Los Altos, CA 94022, and \textbf{Phone}: +1 650 472 1187.} \and Vijay Vaidyanathan\footnote{CEO at Optimal Asset Management, research associate at Edhec-Risk Institute, \textbf{email}: vijay@optimalam.com.}}


\begin{document}

\maketitle


\begin{abstract} %The factor space offers a great diversification potential to investors. Indeed,
The active returns of standard risk factors have very low or negative cross-correlations, and their performance is often complementary across different market regimes. For example, low-volatility stocks tend to perform best precisely at times when small-caps suffer the most and vice versa. As a consequence, combining these and other factors can yield more robust performance and fit the preferences of a larger set of investors than by investing in single factors alone. The potential benefits of constructing factor blends is very large, but the process should have at its core an understanding of the `bad times' of the factors.
\end{abstract}

%Blending ``mean-reverting'' Value and ``trend-following'' Momentum factors has a materially higher Information Ratio than each of its strands. 

\hspace{1cm}

A large body of empirical evidence and economic theory has accumulated over the last few decades supporting the existence of a set of risk factors governing stock returns and that are rewarded in the long-term beyond the standard equity risk premium. The existence of these factors has caused the industry to respond by offering several factor indices and corresponding index trackers that aim to capture the performance of those factors. In turn, this has led investors to rethink asset allocation as a choice among factors rather than individual securities or asset classes. Hence, a natural question to ask is, how should investors allocate among the available factors?

Generally speaking, a defining characteristic of a rewarded risk factor is its \emph{bad times}, that is, the market conditions under which a portfolio of stocks driven by that factor underperform the average (i.e. the market portfolio). The reason that factors are expected to command a higher than average long-term return is because of the excess risk they carry, which is realized during their bad times. Identifying the bad times of factors is thus very important, as they are a central criteria that investors should use to determine the extent and direction to which they want to expose themselves to the risk factors (e.g. investing in Value or Growth stocks), if at all. Determining the right factor allocation should depend not only on the characteristics of the factors but also on the investor's risk aversion, horizon, views, objectives and exposures. For example, an investor with a longer horizon than the average investor in the market, such as a pension fund, may be able to afford investing in assets with higher risk and reward than the market portfolio, such as Value stocks or small cap stocks. However, many investors are not ``single'' factor investors, in the sense that their preferences and beliefs might not be perfectly matched with a single factor. Furthermore, the factors that have survived the scrutiny of researchers have done so because each of them has a significant explanatory power of stock returns beyond the precedent factor model (which started with the one factor CAPM\footnote{The CAPM was introduced by \cite{treynor1961market}, \cite{sharpe1964capital}, \cite{lintner1965valuation} and \cite{mossin1966equilibrium} independently, building on the work of Harry Markowitz on diversification and modern portfolio theory.}), and thus it should not come as a surprise that the correlations among these factors are very low. These facts prompt us to show that combining factors expands the range of risk-return tradeoffs available to investors by exploiting the diversification opportunities in factor space.

% \cite{ang2014asset}

% \label{table:regressionFunds}
\input{RegFunds.tex}

Although the characteristics and the reasons for which standard factor premia exist have been well studied (and debated), much less has been said about how to combine these factors. Nonetheless, investors do not appear to be uninformed on how to tackle this question. In fact, prior to the start of the current Factor Investing revolution, many investment managers and investors were familiar with the economic intuition behind most standard risk factors, and active managers often (intentionally or unintentionally) express their views in the form of traditional investment styles. As a consequence, the performance of many actively managed equity mutual funds can be decomposed to a relatively high level of accuracy as a combination of these style factors. For instance, Table \ref{table:regressionFunds} presents the factor attribution of 7 large US large-cap equity active mutual funds\footnote{These funds are the top 7 large US equity active mutual funds by assets under management and return history available since January 1973 with 3 stars or higher as rated by Morningstar as of 2015-06-08.} regressed on the returns of the market portfolio and returns of three standard long-short equity factors (see Section \ref{sec:FactorsSet} for the description of construction of the factors). The average $R^2$ of the regression across funds is 80\%, and it ranges between 67\% and $93\%$. A returns-based style analysis \citep[see][]{sharpe1992asset} forming long-only factor portfolios (using the high and low sides of the factors\footnote{We refer to the long and short factor portfolios that comprise the long-short factor as the `high' and `low' sides of the factor respectively. Further details are contained in Section \ref{sec:FactorsSet}}) confirm this result: the pseudo $R^2$ (see the right-most column in Table \ref{table:RBFAFunds}) are very similar to the regression $R^2$ in all cases\footnote{In this analysis we solve for the allocation to the set of long-only factor portfolios that minimizes the tracking error relative to each mutual fund. As instrument assets we use the long-only factor portfolios that correspond to the sign of the regression coefficients of the corresponding factors presented in Table \ref{table:regressionFunds} for each of the funds. The pseudo $R^2$ is calculated as $1-\frac{TE^2_m}{\sigma^2_{R_m}}$, where $TE_m$ is the tracking error of the factor-based synthetic fund relative to the original mutual fund, and $\sigma^2_{R_m}$ is the return variance of the mutual fund $m$.}. Furthermore, the total allocation to the factors different from the market portfolio is above 60\% for all the 7 funds. In other words, the estimated allocation to the market portfolio varies from 0\% to 32\%.

These results mean that investors in those active funds have indeed been living in factor space, but perhaps not consciously choosing their positioning within it. In a similar factor analysis of a comprehensive universe of active funds, \cite{kahn2015smart} find a strikingly similar effect. In particular they find that the returns to portfolios consisting of aggregating different active funds are increasingly explained by static exposures to standard factors. In effect, the active risk of the funds is rapidly diversified away as more funds are aggregated, leaving mostly systematic factor returns in the portfolio.

Similar to Factor Investing, another important recent development in investment management known as ``smart beta'' consists of setting the weight of each stock in the portfolio with a systematic rule different from the standard market-cap rule. Popular examples of those rules are Fundamental Indexing \citep[][]{arnott2005fi} and risk-based allocation strategies \citep[see][for a review]{jurczenko2013generalized} such as Minimum Variance optimization. When such rules are applied to a broad universe of stocks, it often results in portfolios with significant exposures to some of the standard equity risk factors. For instance, Fundamental Indexing portfolios are close to Value factor portfolios \citep[][]{blitz2008fundamental}, and Minimum Variance portfolios induce a significant exposure to the LowVol factor, thus also partially explaining the well-documented long-term market outperformance of these ``smart beta'' strategies. 

The standard definition of the return of the Value factor portfolio is the return of ``Value stocks'' weighted either by market cap or less frequently, equal weighted \citep[see for instance][]{fama1993common}. However, in the factor world, stocks are viewed as bundles of factors, and thus choosing a weighting scheme to form factor portfolios is not a seamless choice and needs to be done with caution, as the characteristics of the factors can be significantly modified by the weighting scheme. Thus, in order to capture ``pure'' factor effects, we focus our analysis on market-cap weighted factor portfolios. Indeed, the market cap-weighted portfolio of say, all ``Value stocks'' is the only one that represents the performance of the sum of all the dollars invested in that group of stocks in the market.

%label{table:RBFAFunds}
\input{RBFAFunds.tex}

\section{The Factor Opportunity Set}\label{sec:FactorsSet}

The standard factor premia can be captured by investing in portfolios formed with systematic selections of stocks that share common characteristics, such as a high book-value to market-price ratio (for the Value factor), smaller market capitalization (Size factor), high recent realized returns (Momentum), a low idiosyncratic volatility (LowVol factor)\footnote{Other well known factors correspond to Quality investing \citep[see for instance][]{novy2013other,fama2015five}.}. Although the methodologies vary across commercial factor index providers, the most standard approach is fairly close to the methodologies used in the academic literature, which consists of a two step procedure: first, selecting stocks according to their factor scores (e.g. high book-to-market ratios) and second, weighting the stocks in proportion to the market capitalization of each of the selected stocks\footnote{See for instance the FTSE factor indices methodology at \url{http://www.ftse.com/products/downloads/FTSE_Global_Factor_Index_Series_Methodology_Overview.pdf}. One common difference in methodology is on how the scores for the factors are constructed. For instance FTSE and Russell use both book-to-market ratios and dividend yield to construct a value score, instead of only book-to-market ratio as most academics do.}. Hereafter we use US data on a set of standard factor portfolios over the period 01-1973 to 12-2013 (41 years)\footnote{We start our sample in 1973 to avoid a sudden increase in the total number of stocks induced by the introduction of the NASDAQ stock exchange.}. 

In order to form the Size and Value factor portfolios we use the $2\times 3$ Size and Book-to-Market monthly data from Kenneth French website\footnote{The data source for Fama-French factor portfolios is \url{http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html}}. In order to obtain Value and Growth factor portfolios for the full universe of stocks we aggregate the Large cap and Small cap versions of the Value and Growth portfolios using their market capitalization \citep[instead of equal weighting them as in][]{fama1993common}. This yields market-cap weighted factor portfolios, which avoids an overrepresentation of small cap stocks. We follow a similar methodology to form the Size and Momentum (high and low) factor portfolios.

Hence, in order to obtain the market-cap weighted long-short Size factor we calculate,

\begin{equation*}
size = size\_high - size\_low
\end{equation*}
where
\begin{align*}
size\_high & = wc_{SV} Small Value + wc_{SN} Small Neutral + wc_{SG} Small Growth \\
size\_low & = wc_{BV} Big Value + wc_{BN} Big Neutral + wc_{BG} Big Growth
\end{align*}
and $wc_{SV} = \frac{cp_{SV}}{cp_{SV} + cp_{SN} + cp_{SG}}$ where $cp_{SV}$ is the total market capitalization of all the Small Value stocks as defined in \cite{fama1993common} (the other market cap weights are defined similarly).

The market-cap weighted long-short Value factor is in turn calculated as
\begin{equation*}
value = value\_high - value\_low
\end{equation*}
where
\begin{align*}
value\_high & = pc_{SV} Small Value + pc_{BV} Big Value \\
value\_low & = pc_{SG} Small Growth  + pc_{BG} Big Growth
\end{align*}
and $pc_{SV} = \frac{cp_{SV}}{cp_{SV} + cp_{BV}}$. Similarly, we construct a market-cap weighted momentum factor using the monthly $2\times 3$ Size and Momentum portfolios data from Kenneth French website as
\begin{equation*}
mom = mom\_high - mom\_low
\end{equation*}
where
\begin{align*}
mom\_high & = qc_{SH} Small High + qc_{BH} Big High \\
mom\_low & = qc_{SL} Small Low  + qc_{BL} Big Low
\end{align*}
and $qc_{SH} = \frac{cp_{SH}}{cp_{SH} + cp_{BH}}$, where $cp_{SH}$ is the market capitalization of Small and High Momentum stocks. The latter are the intersection of the stocks with lowest market equity and the highest monthly prior (2-12) return, where the breakpoints defining the top and bottom Momentum stocks are the $30^{th}$ and $70^{th}$ NYSE percentiles of prior returns. The market portfolio and the risk-free asset returns are from Kenneth French website\footnote{Ibid.}.

Following \cite{ang2006cross,ang2009high} in order to define the low-volatility factor, we first rank stocks in the universe (i.e. all common stocks traded in AMEX, NYSE or NASDAQ) and form quintiles according to their idiosyncratic volatility relative the to 3-factor Fama-French model, measured as the standard deviation of the regression residuals, which are estimated using daily excess returns over the past month\footnote{We use total returns (dividends re-invested). The dataset has all stocks from CRSP with share code in 10 or 11 and exchange code in 1, 2 or 3 and for which we have positive book value from Compustat.}. The long-short low-volatility factor (denoted lvol) is
\begin{equation*}
lvol = lvol\_high - lvol\_low
\end{equation*}
where $lvol\_high$ denotes the market-cap weighted quintile portfolio of stocks with the lowest idiosyncratic volatility, and $lvol\_low$ is the market-cap weighted quintile portfolio of stocks with the highest idiosyncratic volatility over the last month.

\input{summaryLS.tex}

Table \ref{table:summaryLS} presents the annualized (arithmetic) average monthly return of the long-short factors aforementioned, their correlation matrix, along with the t-statistics and p-values of the test that the average return spread is significantly different from zero. The excess return is significantly positive at the 90\% confidence level for the size factor, at the 98\% level for the value factor and at the 99\% level for the momentum and low vol factors.

As observed in Table \ref{table:summaryLS}, the lvol factor has a correlation of $-64\%$ with the size factor, while the mom factor has a correlation of $-17\%$ with the value factor. Furthermore the mom and size pair also presents a negative correlation and the lvol and value pair has a correlation close to 0\%, suggesting there is meaningful diversification potential within this long-short factor space.

In practice, many institutional investors face borrowing limits or long-only allocation constraints that prohibit or limit their ability to take short positions. Thus their opportunity set does not include the long-short factors presented in Table \ref{table:summaryLS}. However, they may choose to define their equity portfolio as allocations across the long-only factor portfolios that constitute the long (i.e. high such as the high Value stocks in the case of the Value factor) and short (i.e. low Value a.k.a. Growth stocks in the case of the Value factor) strands of the long-short factors. The return of a portfolio of factors $R_p$ would then be a linear combination of $N$ long-only factor portfolios $F_i$ for $i \in \{1 \dots N\}$, each them representing one side of a factor. Thus for 4 factors, we would have $N=8$, and
\begin{equation}\label{eq:fundDecomp}
R_p = \sum^N_{i=1} w_{i} F_i
\end{equation}
where $w_{i}$ represents the allocation of the portfolio to the factor portfolio $i$ and satisfies $\sum^N_i w_i =1$, and $0 \leq w_i\leq 1$ for $i \in \{1 \dots N\}$.

%\label{table:summary}
%\input{summaryFP.tex}

%Table \ref{table:summary} presents the (geometric) average return, volatility, Sharpe ratio and correlation matrix for the market portfolio and each of the 8 long-only factor portfolios. We observe that all of the long-only factor portfolios present a high correlation with the market portfolio ($91\%$ on average). This implies that all of them have an important component of broad equity market risk. However, it should also be noticed that the high value, high mom and high lvol portfolios present a Sharpe ratio of around 0.5, which constitute an economically meaningful difference relative to the Sharpe ratio of the market portfolio of 0.33, over the 40 years period. The high lvol portfolio presents a performance slightly higher than the market portfolio in the overall period, though not significantly different (as shown in Table \ref{table:summaryE}), but it presents the lowest volatility of all factor portfolios, i.e. $13.6\%$ compared to $16.1\%$ for the market or higher for most factor portfolios.

The performance of most equity portfolios is measured relative to a benchmark that intends to replicate the market portfolio (denoted Mkt). The quantity of interest, often called the \emph{active return} is the difference between the portfolio return and the return of the cap-weighted benchmark Mkt. From equation \eqref{eq:fundDecomp} the active return of our portfolio of factors $R_p$ can be decomposed as
\begin{equation}\label{eq:EfundDecomp}
R_p - \text{Mkt} =  \sum^N_{i=1} w_{i} (F_i - \text{Mkt}), \text{  for  } \sum^N_i w_i =1.
\end{equation}

Thus the active return of a long-only portfolio of long-only factor portfolios can be interpreted as a weighted average of the performance of a set of long-short portfolios, with long positions in the 8 long-only factor portfolios and short the market. In that sense, the long-short portfolios in equation \eqref{eq:EfundDecomp} constitute the investment opportunity set of an investor measuring the performance of the portfolio relative to the market portfolio (or any other given benchmark denoted Mkt).

\input{summaryE.tex}

Table \ref{table:summaryE} presents the annualized (arithmetic) average active return $F_i - \text{Mkt}$ for the 8 factor portfolios, along with their t-statistics and p-values for statistical significance, as well as the Correlations across these long-short portfolios. The average active return is economically and statistically significant for the high size, high value and high mom factor portfolios. Although the high strand of the lvol factor presents the same return (statistically speaking) as the market, its active return presents negative correlations with most of the other active returns, for example the correlation with the excess $size\_high$ portfolio is $-48\%$. Moreover, the active returns of all the factor portfolios have a negative average correlation with the others (e.g. average by column excluding the correlation with itself), and the average correlation across all pairs is $-9\%$.

%Although the long-only factor portfolios present high correlations with the market portfolio (see Table \ref{table:summary} for the summary statistics of the 8 long-only factor portfolios). Notice that lvol has a substantially higher return than its high volatility counterpart

\begin{figure}%[H]
\label{fig:BadTimesRet}
\centering
\includegraphics[scale=0.45, angle=360]{BadTimesRet.pdf}
\includegraphics[scale=0.45, angle=360]{BadTimesRetVol.pdf}
\caption{Annualized Conditional Excess Return and Excess Volatility Relative to the Market of Factor Portfolios Across Regimes. The Left Panel is Based on Market Regimes, while the Right Panel on Value Factor Regimes.}
\end{figure}

Besides the sample correlations indication of the potential diversification benefits of combining these factor portfolios, a more revealing piece of information is what we refer to, as the \emph{bad times analysis}, which consists of comparing the relative performance of the factors across different market regimes. In order to define market regimes, we sort the quarters into quintiles, based on, for instance, the market return over each quarter, so that the highest quintile comprise those quarters with the top 20\% of quarterly market returns. Thus the regimes range from ``Bear'' markets to ``Bull'' markets as the highest. We string together the returns for the non-overlapping quarters that make up each quintile, and annualize the result. The factor portfolio return during each quintile (or regime) equates to the return of being invested in that portfolio only during the quarters of that market regime, and can be interpreted as a set of conditional performances.

Other definitions of regimes can be used depending on the context (for example by conditioning performance on observed levels of volatility, or macroeconomic variables such as inflation, or industrial production) can be useful to some investors. For instance, a pension fund with inflation-linked liabilities would be interested in the performance of the factors under consideration across different inflation environments. Alternatively, a useful (and in our opinion, underutilized) option is to define the bad times of a risk factor according to the relative performance of its two sides. For instance by sorting quarters according to the return difference of Value stocks versus Growth stocks, as discussed in section \ref{sec:ValueMom}, a ``Value Bad Times'' analysis could provide insight into how a portfolio performs in regimes when Growth beats Value; an analysis that should be of particular interest to investors who have already committed allocations to investments that feature a pronounced Value tilt.

Figure \ref{fig:BadTimesRet} presents the regime-conditional performance in terms of excess return and excess volatility relative to the market of the four factor portfolios that corresponds to the high side of the factors. It is striking to note the complementarily of the conditional performance of the two pairs presented in the figure across market regimes. Indeed, the $size\_high$ factor presents a large market underperformance during Bear markets ($-7\%$), and a material outperformance during Bull markets ($16\%$). On the other hand, the performance of the $lvol\_high$ factor portfolio almost mirrors this behavior in Bear and Bull markets. Also, the $mom\_high$ factor portfolio presents an important market outperformance in regimes where the $value\_high$ factor is underperforming $value\_low$ (i.e. when Growth beats Value). This result suggests that combining these factors should produce more stable performance across market regimes.

\section{The Diversification Benefits of Factor Blending}\label{sec:Blending}

The preceding results as well as the intuition behind the economic drivers of these factors suggest that there are important diversification benefits in combining them. In this section we consider some examples of long-only factor blends. To construct the factor blends we equally weight the factor portfolios considered and rebalance the blends every month, to use the same formation period of some of the component factors.

\subsection{Blending LowVol and Small Caps}\label{sec:lvolAndSC}

Despite the attractive returns that small-cap stocks can offer in the long run, a portfolio composed of small-caps alone might be too risky even for many typical long-term investors. Furthermore, some small-cap stocks with a very low valuation might be in distress and represent bad investments even in the long term (a similar effect in Value portfolios is commonly referred as ``the Value trap''). 

Combining small-cap stocks with low-volatility stocks can yield a more stable risk-return profile across market regimes than a pure small-cap portfolio. The reason is that low-volatility stocks tend to perform very well (relative to the average stock) during bad times, precisely when small-cap stocks suffer the most (see Figure \ref{fig:BadTimesRet}). On the other had, low-volatility stocks also tend to underperform during market rallies, which are times during which small-cap stocks tend to outperform the average stock. Diversifying across these should offer superior risk-adjusted returns.

%  Although the decrease in risk relative to a pure small-caps investment is almost guaranteed over time (in absolute terms and relative to the market, i.e. tracking error), 

Of course, whether the complementary behavior across regimes will result in a better return outcome overall is an empirical question that depends on the proportion allocated to each of these two assets. In other words, whether the outperformance of $lvol\_high$ stocks will be higher than the underperformance of $size\_high$ stocks during bad times and whether the outperformance of $size\_high$ stocks will be higher than the underperformance of $lvol\_high$ stocks in good times is unknown a priori.

Hereafter we refer to the blend of the $lvol\_high$ and $size\_high$ factor portfolios as LS. The left panel of Figure \ref{fig:BadTimesBlends} presents the bad times analysis for LS. Unlike the highly variable relative performance of its strands, the LS blend outperforms the market across all market regimes. LS has a lower average volatility than the $size\_high$ factor in all market regimes, and it has a higher return in 3 out of the 5 market regimes. In particular, during Bear markets, its average excess return of $1.6\%$ contrast with the $-7\%$ of $size\_high$. LS also presents a stronger performance than the $lvol\_high$ factor portfolio in 3 out of 5 regimes, and its average market outperformance during Bull markets of $3.4\%$ contrast with the market underperformance of $lvol\_high$, i.e. $-8.8\%$.

\begin{figure}%[H]
\centering
\includegraphics[scale=0.45]{BadTimesRetBlends.pdf}
\includegraphics[scale=0.45]{BadTimesRetVolBlends.pdf}
\caption{Annualized Conditional Excess Return and Excess Volatility Relative to Mkt of Factor Portfolios and Factor Blends Across Regimes. The Left Panel is Based on Market Regimes, while the Right Panel on Value Factor Regimes.}
\label{fig:BadTimesBlends}
\end{figure}

\subsection{Blending Value and Momentum} \label{sec:ValueMom}

Consider a second example, a blend of value and momentum portfolios. Value investing says that stocks with low prices relative to fundamentals (Value stocks) outperform stocks with high prices relative to fundamentals (Growth stocks) over long periods. Thus, a Value strategy is similar to a contrarian one that expects prices of stocks with high book to market values to revert closer to fundamental value. Momentum is in turn a strategy that invest in stocks for which prices have already gone up recently, so it is in some sense, the opposite in philosophy of a Value strategy. Hence it is not surprising that the (high) Value and Momentum factor portfolios present visibly complementary behavior, as shown in the right panel of Figure \ref{fig:BadTimesBlends} which presents the excess return and volatility of the $value\_high$ and $momentum\_high$ factor portfolios across different Value factor regimes (in this case quarters are sorted on the relative return of Value vs. Growth factor portfolios, i.e. Value Bad Times).

In the right panel of Figure \ref{fig:BadTimesBlends} we present the bad times analysis for a 50/50 blend of the $value\_high$ and $mom\_high$ factor portfolios, which we denote VM. As suggested by the complementary regime performance of these two factors, the blend has a lower volatility than the $mom\_high$ while it benefits from the market outperformance of the latter across most Value regimes. On the other hand, the $value\_high$ underperforms VM in 3 out of the 5 market regimes.

Table \ref{table:perfBlends} presents the summary performance statistics over the full sample period of all the long-only factor portfolios, the LS and VM blends, as well as two extra blends combining three (LMV) or the four high sides of the factors (LMVS).

Perhaps one of the most relevant measures of risk-adjusted performance for benchmarked portfolios is the active return achieved per unit of active risk undertaken, which is commonly measured with the Information Ratio (the ratio of Active Premium and Tracking Error). Indeed, the main advantage of blending is observed in terms of Information Ratio (IR). For example, as observed in Table \ref{table:perfBlends}, the $LS$ blend has an information ratio of $0.41$ which is more than three times the IR of its strand $lvol\_high$ (i.e. $0.12$) and almost twice as high as the IR of its other strand, $size\_high$ (i.e. $0.24$). Similar large improvements in Information Ratio are observed for all other blends compared to their constituent factor portfolios (see for instance 0.79 for VM compared to $0.45$ and $0.58$). The improvement in terms of Information Ratio is induced mainly by an important reduction of Tracking Error without a corresponding reduction in performance, which is a consequence of putting together different subsets of the stocks in each factor strand, which taken all together compose the market portfolio. 

In terms of absolute terms, interestingly, we observe that the Sharpe ratio of each of the blends are about the same or higher than each of its constituents. For example, the VM blend presents a Sharpe ratio of $0.56$ compared to $0.53$ and $0.52$ for $value\_high$ and $mom\_high$ respectively, and at the same time VM presents an average return of $14.2\%$ compared to $14.2\%$ and $13.9\%$ for  $mom\_high$ and $value\_high$ respectively.

Besides the widely used tracking error, an alternative measure of active risk is the maximum relative drawdown, which is the maximum drawdown of the relative value of an investment with respect to its benchmark\footnote{The relative value is the ratio of the value of the strategy and the value of the benchmark, though as if the value of the strategy is measured in number of shares of the benchmark instead of dollars. For more details on the Maximum Relative Drawdown definition see \cite{mantilla2014dynamic}.}. This is similar to the maximum drawdown of a long-short portfolio long the strategy and short the benchmark. As observed in Table \ref{table:perfBlends}, blending factor portfolios also yields material improvements in terms of maximum relative drawdown, as all of the blends considered have a lower Max Relative DD than all of its factor constituents. For instance, the LMVS has the lowest Relative Max DD, $24\%$ compared with the corresponding strands for which it ranges between $32\%$ and $77\%$. We also calculate the Relative Calmar ratio (Active Premium divided by Max Relative DD) and find again that each of the blends have a higher relative risk-adjusted ratio than its factor constituents. Most remarkably, the blend LMVS has the highest Relative Calmar ratio, i.e. 12\%, which dwarfs the ratios of its individual factor components that range between 2\% and 6\%.

% \label{table:perfBlends}
\input{perfBlends.tex}

\section{Conclusion}

Individual factor portfolios offer excess returns that are available for investors who are willing to take on a larger share of that factor risk than the average investor in the market. Unfortunately, these excess returns display significant time variation in both magnitude and sign, resulting in protracted periods of `bad times' for each individual factor. Understanding the bad times of the each of the factors not only helps investors decide which factors are an appropriate fit for them but also how to combine those factors. Furthermore, we find that the risk-return profile and conditional performance of some factor blends can substantially differ from its factor constituents, thus enlarging the opportunity set for investors. As we illustrate, blending of standard factor portfolios present large diversification benefits resulting in superior ``active'' risk-adjusted returns compared to single factor portfolios.

% the standard factor portfolios have very different bad times, thus combining them yield large diversification benefits in terms of risk adjusted active return (aka Information Ratio).


\newpage

\bibliography{SBbib.bib}
%\bibliographystyle{apalike}
\bibliographystyle{chicago}

\end{document}